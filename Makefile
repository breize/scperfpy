## Makefile
pythonfiles= *.py modules/*.py
pylintopts=--disable=C0301,R0801 --good-names=i,j,k,t

flake8: $(pythonfiles)
	flake8 --ignore=E501 $(pythonfiles)

pylint: $(pythonfiles)
	pylint $(pylintopts) $(pythonfiles)

.PHONY: lint test help install

.DEFAULT_GOAL := help

lint: flake8 pylint ## code linting (checking)

test: lint ## run code tests

install: ## install requirements (use root or virtualenv!)
	@pip3 install -r requirements.txt

help: ## this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "%-30s %s\n", $$1, $$2}'


''' DsmConnection: connection for one DataCollector '''
import logging
import json
import requests
import urllib3
urllib3.disable_warnings()


def format_line(tags, prefix, item, timestamp):
    '''
        Format one Dict entry as string for line_protocol

        <measurement>[,<tag_key>=<tag_value>[,<tag_key>=<tag_value>]] <field_key>=<field_value>[,<field_key>=<field_value>] [<timestamp>]
    '''
    known_keys = ['time', 'scSerialNumber', 'scName', 'objectType', 'instanceId', 'instanceName']
    measurement = prefix + item['objectType']
    for t in ['scSerialNumber', 'scName', 'instanceId', 'instanceName']:
        tags[t] = str(item[t]).replace(' ', '_')

    # filter out known keys, everything else is actual fields
    values = dict(filter(lambda x: (x[0] not in known_keys), item.items()))

    # stringify tags and values
    fieldsstr = ','.join(list(map(lambda x: ('%s=%.3f' % (x[0], x[1])), values.items())))
    tagstr = ','.join(list(map(lambda x: ('%s=%s' % (x[0], x[1])), tags.items())))

    return f'{measurement},{tagstr} {fieldsstr} {timestamp}\n'


class DsmConnection():  # pylint: disable=too-many-instance-attributes
    ''' REST connection for one DSM '''

    # defaults
    PORT = 3033
    BASE_URL = 'https://%s:%s/api/rest'
    VERIFY = False
    HEADERS = {
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json',
        'x-dell-api-version': '2.0'
    }
    METRICS = {
        'controller': 'GetLatestControllerIoUsage',
        'disk': 'GetLatestDiskIoUsage',
        'system': 'GetLatestIoUsage',
        'server': 'GetLatestServerIoUsage',
        'volume': 'GetLatestVolumeIoUsage'
    }

    def __init__(self, config, global_config):
        ''' constructor '''
        self.server = ''
        self._user = ''
        self._password = ''
        self._port = DsmConnection.PORT
        self._headers = DsmConnection.HEADERS
        self._verify = DsmConnection.VERIFY
        self._connection = None
        self._dsm_instance = 0
        self._prefix = ''
        self.tags = dict()
        self.sc_systems = list()

        self.apply_config(config, global_config)
        self._base_url = DsmConnection.BASE_URL % (self.server, self._port)
        self.login()

    def apply_config(self, config, global_config):
        ''' apply configuration '''
        for elem in ['dsm_server', 'dsm_user', 'dsm_password', 'tags']:
            if elem not in config:
                raise ValueError(elem + ' is required')
        self.server = config['dsm_server']
        self._user = config['dsm_user']
        self._password = config['dsm_password']
        self._prefix = global_config['metric_prefix']

        # process tags from string
        for item in config['tags'].split(','):
            (key, val) = item.split('=')
            self.tags[key] = val

        if 'port' in config:
            try:
                self._port = int(config['port'])
            except ValueError as err:
                raise ValueError(config['port'] + ' must be integer value') from err

    def _post(self, url, payload):
        ''' execute POST request '''
        full_url = self._base_url + url
        data = json.dumps(payload, ensure_ascii=False).encode('utf-8')
        return self._connection.post(full_url, data=data, headers=self._headers, verify=self._verify)

    def _get(self, url):
        ''' execute GET request '''
        full_url = self._base_url + url
        return self._connection.get(full_url, headers=self._headers, verify=self._verify)

    def login(self):
        ''' create connection and login '''
        self._connection = requests.Session()
        self._connection.auth = (self._user, self._password)
        result = self._post('/ApiConnection/Login', {})
        logging.info('Connected to %s', result.json()['hostName'])
        self.get_basic_system_info()

    def get_basic_system_info(self):
        ''' basic system info mainly for info output '''
        result = self._get('/ApiConnection/ApiConnection').json()
        if 'connected' not in result or result['connected'] is not True or 'instanceId' not in result:
            logging.debug(result)
            raise ValueError('Invalid ApiConnection response from DSM')
        self._dsm_instance = result['instanceId']
        logging.info('DSM instance id is: %s', self._dsm_instance)

        url = f'/ApiConnection/ApiConnection/{self._dsm_instance}/StorageCenterList'
        self.sc_systems = list()
        for i in self._get(url).json():
            if i['connected'] is False or i['status'] == 'Down':
                logging.debug(i)
                raise ValueError(f"SC serial {i['serialNumber']} {i['scName']} shows connected={i['connected']}, status={i['status']}")
            system = {
                'instanceId': i['instanceId'],
                'name': i['name'],
                'version': i['version'],
                'serial': i['serialNumber'],
                'serviceTag': i['serviceTag']
            }
            self.sc_systems.append(system)
            logging.info(
                'Found SC %s version=%s,serial=%s,serviceTag=%s',
                system['name'], system['version'], system['serial'], system['serviceTag']
            )
            ctls = self._get('/StorageCenter/StorageCenter/' + i['instanceId'] + '/ControllerList')
            for ctl in ctls.json():
                logging.info(
                    'Controller name=%s,serial=%s,model=%s',
                    ctl['name'],
                    ctl['scSerialNumber'],
                    ctl['model']
                )
        if not self.sc_systems:
            raise ValueError('Connected to DSM ' + self.server + ' but found no SC systems')

    def generate_output(self, metrics, timestamp):
        ''' generate output data
        '''
        result = ''
        logging.debug('%s: %d SC systems to process', self.server, len(self.sc_systems))
        for system in self.sc_systems:
            i = system['instanceId']
            for measurement in metrics[i]:
                values = metrics[i][measurement]
                if isinstance(values, list):
                    logging.debug('%s %s: %d rows', system['name'], measurement, len(values))
                    for item in values:
                        result += format_line(self.tags, self._prefix, item, timestamp)
                else:
                    logging.debug('%s %s: %d values', system['name'], measurement, len(values))
                    result += format_line(self.tags, self._prefix, values, timestamp)
        return result

    def collect_metrics(self, timestamp):
        ''' collect all metrics (latest set) '''
        if not self.sc_systems:
            raise ValueError('Query metrics called for DSM ' + self.server + ' but have no SC systems')
        metrics = dict()
        for system in self.sc_systems:
            i = system['instanceId']
            metrics[i] = dict()
            for metric in DsmConnection.METRICS:
                path = DsmConnection.METRICS[metric]
                url = f'/StorageCenter/StorageCenter/{i}/{path}'
                metrics[i][metric] = self._post(url, {}).json()
        return self.generate_output(metrics, timestamp)

    def __exit__(self, exc_type, exc_value, exc_traceback):
        ''' logout / close connection '''
        if self._connection:
            logging.info('Logging out from DSM %s', self.server)
            self._post('/ApiConnection/Logout', {})

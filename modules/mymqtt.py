''' my mytt: wrapper for mqtt '''
import logging
import time

import paho.mqtt.client


class MyMqtt():
    ''' mqtt wrapper class '''

    def __init__(self):
        ''' constructor '''
        self.server = ''
        self._port = ''
        self._topic = ''
        self._topic_agent = ''
        self._reconnect_interval = 24 * 3600
        self._mqtt = paho.mqtt.client.Client()
        self._last_reconnect = time.time()

    def publish(self, servername, data):
        ''' MQTT publish '''
        try:
            topic = self._topic + '/' + servername + '/' + self._topic_agent
            logging.info('Publishing to %s:%s, %d bytes', self.server, topic, len(data))
            self._mqtt.publish(topic, payload=data)
            logging.debug('Published.')
            if time.time() - self._last_reconnect > self._reconnect_interval:
                logging.info('Scheduled reconnect to MQTT %s', self.server)
                self._mqtt.disconnect()
                self._mqtt.reconnect()
                self._last_reconnect = time.time()
        except (ValueError, RuntimeError) as err:
            logging.error('Error during publish: %s', str(err))
            self._mqtt.reconnect()

    def setup(self, config):
        ''' initial setup '''
        for item in ['mqtt_server', 'mqtt_port', 'mqtt_topic']:
            if (item not in config) or (config[item] == ''):
                raise ValueError(f'Missing config setting "{item}"')
        self.server = config['mqtt_server']
        self._port = int(config['mqtt_port'])
        self._topic = config['mqtt_topic']
        self._topic_agent = config['mqtt_topic_agent']
        self._reconnect_interval = config['mqtt_reconnect']
        logging.info('Connecting to MQTT broker at %s', self.server)
        self._mqtt.enable_logger(logging)
        self._mqtt.on_log = self.on_log
        self._mqtt.connect(self.server, self._port)

    def on_log(self, client, userdata, level, buf):  # pylint: disable=unused-argument
        '''MQTT callback function. Used to trigger reconnect on errors '''
        if level == paho.mqtt.client.MQTT_LOG_ERR:
            logging.error('Paho MQTT error: %s - scheduling reconnect', buf)
            self._last_reconnect = 0

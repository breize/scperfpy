''' config module '''
import os
import sys


class AppConfig():
    ''' config class to hold application config '''

    # class variables as defaults
    CONFIG_FILE = 'scperfpy.cfg'
    CFGDEFAULT = {
        'global': {
            'mqtt_server': '',
            'mqtt_port': 1883,
            'mqtt_topic': '',
            'mqtt_topic_agent': '',
            'mqtt_reconnect': 2 * 3600,  # 2 hours
            'interval': 60,
            'error_interval': 300,
            'max_retries': 30,
            'logfile': 'scperfpy.log',
            'metric_prefix': '',
            'verbose': False,
            'debug': False,
            'daemonize': False
        },
        'dsm': {
            'dsm_server': '',
            'dsm_port': 3033,
            'dsm_user': 'sc',
            'dsm_password': 'sc',
            'tags': {}
        }
    }

    def __init__(self):
        ''' constructor. just apply default config '''
        self.cfg = AppConfig.CFGDEFAULT.copy()
        self.cfg['dsm'] = list()
        self.config_file = AppConfig.CONFIG_FILE

    def update_config_from_file(self):
        ''' read configfile and update config '''
        if not os.path.isfile(self.config_file):
            sys.exit('Configfile ' + self.config_file + ' not found')
        try:
            with open(self.config_file, 'r') as f_h:
                content = f_h.read()
        except (OSError, IOError) as err:
            sys.exit(f'Uable to open {self.config_file}: {err.strerror}')

        section = None
        dsm = None
        for line in content.split('\n'):
            if not line or line[0] == '#':
                continue
            if line[0] == '[':
                section = line[1:-1]
                if section == 'dsm':
                    if dsm:
                        self.cfg['dsm'].append(dsm)
                    dsm = AppConfig.CFGDEFAULT['dsm'].copy()
                continue
            if line.find('=') > -1:
                (key, value) = line.split('=', 1)
                if section == 'dsm':
                    dsm[key] = value
                else:
                    self.cfg[section][key] = value
        # add last element
        if dsm:
            self.cfg['dsm'].append(dsm)

    def update_config_from_args(self, args):
        ''' update config from commandline arguments '''
        if args.interval:
            self.cfg['global']['interval'] = args.interval
        if args.server:
            self.cfg['global']['mqtt_server'] = args.server
        if args.port:
            self.cfg['global']['mqtt_port'] = args.port
        if args.debug:
            self.cfg['global']['debug'] = args.debug
        if args.verbose:
            self.cfg['global']['verbose'] = args.verbose
        if args.logfile:
            self.cfg['global']['logfile'] = args.logfile

    def setup(self, args):
        ''' constructor '''
        if args.cfgfile:
            self.config_file = args.cfgfile
        self.update_config_from_file()
        self.update_config_from_args(args)

        # verify integer args
        for elem in ['mqtt_port', 'interval', 'error_interval', 'max_retries', 'mqtt_reconnect']:
            try:
                self.cfg['global'][elem] = int(self.cfg['global'][elem])
            except ValueError:
                sys.exit(elem + ' must be integer value')

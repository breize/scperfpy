#!/usr/bin/env python3
''' scperf.py: collect performance metrics from SC/Compellent systems
'''
import argparse
import logging
import os
import signal
import sys
import threading
import time

from modules.config import AppConfig
from modules.dsm import DsmConnection
from modules.mymqtt import MyMqtt

# GLOBALS
config = AppConfig()
mqtt = MyMqtt()
threads = list()
main_stop_event = threading.Event()
publish_lock = threading.Lock()


def signal_handler(sig, frame):  # pylint: disable=unused-argument
    ''' handler for graceful exit
    '''
    logging.info('Stopping (can take a minute)')
    main_stop_event.set()
    for t in threads:
        t.join()
    logging.info('Done')


def try_reconnect(dsm, stop_event):
    ''' try to reconnect to dsm '''
    retries = 1
    connected = False
    while (retries < config.cfg['global']['max_retries']) and (not connected) and (not stop_event.is_set()):
        time.sleep(config.cfg['global']['error_interval'])
        logging.error('No connection to %s, waiting', dsm.server)
        try:
            dsm.login()
            connected = True
        except Exception:  # pylint: disable=broad-except
            logging.error('Reconnect(%s) to %s did not work, retrying', retries, dsm.server)
            retries += 1
    if not connected:
        logging.critical('Max retries for %s exceeded. Bailing out', dsm.server)
        sys.exit(1)
    return dsm


def thread_loop(dsm, stop_event):
    ''' loop with one dsm instance '''
    interval = config.cfg['global']['interval']
    drift_second = int(time.time()) % 60
    if drift_second > 57 or drift_second < 3:
        logging.info('Sleep 4 seconds for minute rollover')
        time.sleep(4)
        drift_second = int(time.time()) % 60
    while not stop_event.is_set():
        try:
            started = time.time()
            metrics = dsm.collect_metrics(int(started))
            with publish_lock:
                mqtt.publish(dsm.server, metrics)
            drifter = 0
            if (int(started) % 60) != drift_second:
                drifter = 1
                logging.info('Adjusting sleep for drift second')
            worktime = time.time() - started
            logging.info('%s iteration took %.3f seconds', dsm.server, worktime)
            time.sleep(interval - worktime % interval - drifter)
        except Exception as err:  # pylint: disable=broad-except
            logging.debug('Exception occured: %s', str(err))
            logging.debug('Try to reconnect to DSM %s', dsm.server)
            dsm = try_reconnect(dsm, stop_event)


def dsm_login_persistent(dsmconfig, stop_event):
    ''' try to login to one dsm instance '''
    retries = 1
    connected = False
    while (retries < config.cfg['global']['max_retries']) and (not connected) and (not stop_event.is_set()):
        try:
            dsm = DsmConnection(dsmconfig, config.cfg['global'])
            connected = True
        except Exception as err:  # pylint: disable=broad-except
            logging.error('Failed to connect to DSM %s: %s', dsmconfig['dsm_server'], str(err))
            time.sleep(config.cfg['global']['error_interval'])
            retries += 1
    if not connected:
        logging.critical('Max retries for %s exceeded. Bailing out', dsmconfig['dsm_server'])
        sys.exit(1)
    return dsm


def thread_main(dsmconfig, stop_event):
    ''' main function per worker thread '''
    try:
        dsm = dsm_login_persistent(dsmconfig, stop_event)
        thread_loop(dsm, stop_event)
    except Exception as err:  # pylint: disable=broad-except
        logging.critical('Exception occured: %s', str(err))


def setup_logging(cfg):
    ''' setup logging configuration '''
    loglevel = logging.INFO
    if cfg['verbose']:
        loglevel = logging.DEBUG
    logconfig = {
        'level': loglevel,
        'format': '%(asctime)s:%(levelname)s:%(message)s',
        'datefmt': '%d.%m.%Y-%H:%M:%S'
    }
    if not cfg['debug']:
        logconfig['filename'] = cfg['logfile']
    logging.basicConfig(**logconfig)


def daemonize():
    ''' daemonize the program '''
    try:
        pid = os.fork()
        if pid > 0:
            sys.exit(0)
    except OSError as err:
        sys.stderr.write('Could not daemonize: %d %s\n' % (err.errno, err.strerror))
        sys.exit(1)
    os.chdir('/')
    os.setsid()
    sys.stdout.flush()
    sys.stderr.flush()
    stdi = open('/dev/null', 'r')  # pylint: disable=consider-using-with
    stdo = open('/dev/null', 'a+')  # pylint: disable=consider-using-with
    stde = open('/dev/null', 'a+')  # pylint: disable=consider-using-with
    os.dup2(stdi.fileno(), sys.stdin.fileno())
    os.dup2(stdo.fileno(), sys.stdout.fileno())
    os.dup2(stde.fileno(), sys.stderr.fileno())


def command_line_args():
    ''' retrieve command line args, if any '''
    parser = argparse.ArgumentParser(description='Collect SC/Compellent performance metrics')
    parser.add_argument('--interval', help='interval in seconds')
    parser.add_argument('--server', help='mqtt server')
    parser.add_argument('--port', help='mqtt port (default 1883)')
    parser.add_argument('--cfgfile', help='config file, default is scperfpy.cfg')
    parser.add_argument('--logfile', help='logfile, default is scperfpy.log')
    parser.add_argument('--debug', action='store_true', help='run without daemonizing')
    parser.add_argument('-v', '--verbose', action='store_true', help='verbose logging')
    args = parser.parse_args()
    return args


###################################################
# MAIN
###################################################
def main():
    ''' main program starts here '''
    # install graceful exit handler
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    args = command_line_args()
    config.setup(args)

    if len(config.cfg['dsm']) == 0:
        logging.error('No DSM instances found in config. Please add [dsm] sections')
        sys.exit(1)

    setup_logging(config.cfg['global'])
    if config.cfg['global']['daemonize'] and not config.cfg['global']['debug']:
        daemonize()

    try:
        mqtt.setup(config.cfg['global'])
    except Exception as err:  # pylint: disable=broad-except
        logging.error('Failed to connect to MQTT %s: %s', mqtt.server, str(err))
        sys.exit(1)

    # create one thread for each dsm instance
    for dsmconfig in config.cfg['dsm']:
        t = threading.Thread(target=thread_main, args=(dsmconfig, main_stop_event))
        t.start()
        threads.append(t)


if __name__ == '__main__':
    main()
